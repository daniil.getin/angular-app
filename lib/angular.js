function SmallAngular() {
  const rootScope = {};
  const directives = {};

  function directive(name, fn) {
    directives[name] = fn;

    return this;
  }

  const compile = node => {
  // your code
  };

  rootScope.$apply = () => {
  // your code
  };

  this.createApp = appName => {
    this.appName = appName;
    return this;
  };

  this.$whatch = () => {
  // your code
  };

  this.bootstrap = (node = document.querySelector('ng-app')) => {
  // your code
  };

  this.directive = directive;
  // this.controller = controller;
  // this.servise = servise;
  // initBaseDirectives();
  this.bootstrap();
}

window.angular = new SmallAngular();
